<?php
/*
Author:			edward_plainview
Author Email:	edward@plainviewplugins.com
Author URI:		https://plainviewplugins.com
Description:	Handle gun range lane bookings.
Plugin Name:	Plainview Lane Booking
Plugin URI:		https://wordpress.org/plugins/plainview-lane-booking
Text Domain:	pvlb
Version:		0.02
*/

namespace plainview\lane_booking
{
	require_once( __DIR__ . '/vendor/autoload.php' );

	class Plainview_Lane_Booking
		extends \plainview\pvlb_sdk\wordpress\base
	{
		/**
			@brief		Plugin version.
			@since		2019-03-24 18:31:04
		**/
		public $plugin_version = PLAINVIEW_LANE_BOOKING_VERSION;

		use \plainview\pvlb_sdk\wordpress\traits\debug;

		use email_trait;
		use frontend_trait;
		use post_editor_trait;
		use menu_trait;
		use misc_trait;
		use settings_trait;
		use tools_trait;
		use user_trait;

		/**
			@brief		Constructor.
			@since		2017-12-07 19:31:43
		**/
		public function _construct()
		{
			$this->load_language( 'pvlb' );
			$this->init_frontend_trait();
			$this->init_email_trait();
			$this->init_menu_trait();
			$this->init_misc_trait();
			$this->init_post_editor_trait();
			$this->init_user_trait();
//			$this->add_action( 'wp_enqueue_scripts' );
			if ( ! session_id() )
				session_start();
			ob_start();
		}
	}
}

namespace
{
	define( 'PLAINVIEW_LANE_BOOKING_VERSION', 0.02 );
	/**
		@brief		Return the instance of ThreeWP Broadcast.
		@since		2014-10-18 14:48:37
	**/
	function Plainview_Lane_Booking()
	{
		return \plainview\lane_booking\Plainview_Lane_Booking::instance();
	}

	new plainview\lane_booking\Plainview_Lane_Booking();
}
