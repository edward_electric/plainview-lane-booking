<?php

namespace plainview\pvlb_sdk\wordpress\object_stores;

/**
	@brief		Master class for Wordpress stores.
	@since		2016-01-02 01:19:06
**/
trait Store
{
	use \plainview\pvlb_sdk\object_stores\Store;
}
