<?php

namespace plainview\pvlb_sdk\table;

/**
	@brief		Cell of type TH.
	@since		20130430
**/
class th
	extends cell
{
	public $tag = 'th';
}
