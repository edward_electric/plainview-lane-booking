<?php

namespace plainview\pvlb_sdk\table;

/**
	@brief		Body section of table.
	@since		20130430
	@version	20130430
**/
class body
	extends section
{
	public $tag = 'tbody';
}
