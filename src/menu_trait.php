<?php

namespace plainview\lane_booking;

/**
	@brief		Handle menu things.
	@since		2019-03-24 18:35:36
**/
trait menu_trait
{
	/**
		@brief		Init menu trait.
		@since		2019-03-24 18:35:41
	**/
	public function init_menu_trait()
	{
		$this->add_action( 'admin_menu' );
	}

	/**
		@brief		Add ourselves to the menu.
		@since		2019-03-24 18:36:00
	**/
	public function admin_menu()
	{
		add_submenu_page(
			'options-general.php',
			// Page heading
			__( 'Plainview Lane Booking', 'pvlb' ),
			// Menu item name
			__( 'Lane Booking', 'pvlb' ),
			apply_filters( 'plainview_lane_booking_admin_menu_capability', 'manage_options' ),
			'plainview_lane_booking',
			[ &$this, 'admin_menu_tabs' ]
		);
	}

	/**
		@brief		Show our admin tabs.
		@since		2019-03-24 18:37:45
	**/
	public function admin_menu_tabs()
	{
		$tabs = $this->tabs();

		$tabs->tab( 'settings' )
			->callback_this( 'settings' )
			// Tab heading
			->heading( __( 'Plainview Lane Booking Settings', 'pvlb' ) )
			// Name of tab
			->name( __( 'Settings', 'pvlb' ) );

		$tabs->tab( 'users' )
			->callback_this( 'users_import' )
			// Tab heading
			->heading( __( 'Plainview Lane Booking Users', 'pvlb' ) )
			// Name of tab
			->name( __( 'Users', 'pvlb' ) );

		$tabs->tab( 'tools' )
			->callback_this( 'tools' )
			// Tab heading
			->heading( __( 'Plainview Lane Booking Tools', 'pvlb' ) )
			// Name of tab
			->name( __( 'Tools', 'pvlb' ) );

		$tabs->tab( 'uninstall' )
			->callback_this( 'admin_uninstall' )
			// Tab heading
			->heading( __( 'Uninstall Plainview Lane Booking', 'pvlb' ) )
			// Name of tab
			->name( __( 'Uninstall', 'pvlb' ) )
			->sort_order( 90 );		// Always last.

		echo $tabs->render();
	}
}
