<?php

namespace plainview\lane_booking;

/**
	@brief		E-mail related.
	@since		2019-03-29 21:54:49
**/
trait email_trait
{
	/**
		@brief		Create a page for sending to users with the completed timetable.
		@since		2019-03-25 12:23:29
	**/
	public function create_booking_done_email_page()
	{
		$page_id = wp_insert_post( [
			'post_content' => 'Welcome to [event_name] on [event_date_time].

[plainview_lane_booking_schedule]

See you on the range!

--
Plainview Lane Booking
',
			'post_status' => 'private',
			'post_title' => 'Lane booking for [event_name], [event_date_time]',
			'post_type' => 'page',
		] );
		return $page_id;
	}

	/**
		@brief		Create a page that is sent to newly-imported users.
		@since		2019-03-29 21:27:01
	**/
	public function create_user_imported_email_page()
	{
		$page_id = wp_insert_post( [
			'post_content' => 'Welcome to Plainview Lane Booking!

A user has been created on [site_name_with_url].

You can now log in and participate in events.

--
Plainview Lane Booking
',
			'post_status' => 'private',
			'post_title' => 'Welcome to [site_name]',
			'post_type' => 'page',
		] );
		return $page_id;
	}

	/**
		@brief		Return the shortcodes used in e-mails.
		@since		2019-04-06 11:05:58
	**/
	public function get_email_shortcodes()
	{
		return [
			'site_name' => get_option( 'blogname' ),
			'site_name_with_url' => sprintf( '<a href="%s">%s</a>', get_option( 'blogname' ), site_url() ),
		];
	}

	/**
		@brief		Init the trait.
		@since		2019-04-06 11:45:57
	**/
	public function init_email_trait()
	{
		$this->add_action( 'pvlb_email_before_send' );
	}

	/**
		@brief		Handle the action that is called before the e-mail is sent.
		@since		2019-04-06 11:41:35
	**/
	public function pvlb_email_before_send( $action )
	{
		if ( $action->is_finished() )
			return;

		if ( $action->apply_css )
		{
			// Load the CSS from disk.
			$css = file_get_contents( __DIR__ . '/../css/email.css' );
			$body = $action->mail->Body;
			$body .= '<style>' . $css . '</style>';
			$action->mail->Body = $body;
		}
	}

	/**
		@brief		Convert a post to a PHPMailer object.
		@since		2019-04-06 11:15:15
	**/
	public function post_to_mail( $post )
	{
		$mail = $this->mail();
		$mail->from( $this->get_local_option( 'email_address' ), $this->get_local_option( 'email_name' ) );
		$mail->subject( $post->post_title );
		$mail->text = $post->post_content;
		$mail->html( wpautop( $post->post_content ) );
		return $mail;
	}

	/**
		@brief		Replace any shortcodes found in the e-mail.
		@since		2019-04-06 11:09:17
	**/
	public function replace_email_shortcodes( $mail, array $shortcodes )
	{
		$subject = $mail->Subject;
		$subject = $this->replace_shortcodes( $subject, $shortcodes );
		$mail->subject( $subject );

		$text = $mail->text;
		$text = $this->replace_shortcodes( $text, $shortcodes );
		$mail->html( wpautop( $text ) );
		return $mail;
	}

	/**
		@brief		Send the schedule e-mail.
		@since		2019-04-06 11:04:02
	**/
	public function send_schedule_email( $state, array $emails )
	{
		$page_id = $this->get_local_option( 'email_schedule_page_id' );
		if ( ! $page_id )
			return;

		$shortcodes = $this->get_email_shortcodes();
		$shortcodes[ 'event_name' ] = $state->get_event_name();
		$time = $state->get_event_time();
		$time = $state->get_time_offset( $time );
		$shortcodes[ 'event_date' ] = date_i18n( get_option( 'date_format' ), $time );
		$shortcodes[ 'event_date_time' ] = date_i18n( get_option( 'date_format' ) . ' ' . get_option( 'time_format' ), $time );
		$shortcodes[ 'event_time' ] = date_i18n( get_option( 'time_format' ), $time );
		$shortcodes[ 'plainview_lane_booking_schedule' ] = $state->groups()->get_schedule_html();

		$post = get_post( $page_id );
		$mail = $this->post_to_mail( get_post( $page_id ) );
		$mail->to( $this->get_local_option( 'email_address' ), $this->get_local_option( 'email_name' ) );
		$this->replace_email_shortcodes( $mail, $shortcodes );
		foreach( $emails as $email_address => $email_name )
			$mail->bcc( $email_address, $email_name );

		$action = $this->new_action( 'email_before_send' );
		$action->mail = $mail;
		$action->execute();

		$mail->send();
	}

	/**
		@brief		Send the e-mail to the newly imported Wordpress user.
		@since		2019-03-31 09:17:55
	**/
	public function send_user_imported_email( $user )
	{
		$page_id = $this->get_local_option( 'email_user_imported_page_id' );
		if ( ! $page_id )
			return;

		$shortcodes = $this->get_email_shortcodes();

		$adt_rp_key = get_password_reset_key( $user );
		$shortcodes[ 'password_reset_link' ] = site_url("wp-login.php?action=rp&key=$adt_rp_key&login=" . rawurlencode( $user->data->user_login ), 'login' );

		$post = get_post( $page_id );
		$mail = $this->post_to_mail( get_post( $page_id ) );
		$mail->to( $user->data->user_email, $user->username );

		$this->replace_email_shortcodes( $mail, $shortcodes );

		$action = $this->new_action( 'email_before_send' );
		$action->mail = $mail;
		$action->execute();

		$mail->send();
	}
}
