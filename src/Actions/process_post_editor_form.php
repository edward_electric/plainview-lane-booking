<?php

namespace plainview\lane_booking\Actions;

/**
	@brief		Allow other plugins to process the post editor form POST.
	@since		2019-04-27 19:04:14
**/
class process_post_editor_form
	extends prepare_post_editor_form
{
}
