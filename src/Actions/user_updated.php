<?php

namespace plainview\lane_booking\Actions;

/**
	@brief		A user was updated.
	@since		2019-03-29 08:43:13
**/
class user_updated
	extends Action
{
	/**
		@brief		IN: The \plainview\lane_booking\Users\User object.
		@since		2019-03-31 09:08:11
	**/
	public $user;

	/**
		@brief		IN: The WP_User object that was just updated.
		@since		2019-03-29 08:43:43
	**/
	public $wp_user;
}
