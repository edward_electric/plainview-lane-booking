<?php

namespace plainview\lane_booking\Actions;

/**
	@brief		Convert a participant to html to be used in the post editor.
	@details	Generally only the content_divs should be modified.
	@since		2019-05-04 20:21:28
**/
class participant_to_container_html
	extends Action
{
	/**
		@brief		IN: The container div.
		@since		2019-05-04 20:21:46
	**/
	public $container;

	/**
		@brief		IN/OUT: An array of div objects for the content in the container.
		@since		2019-05-04 20:21:46
	**/
	public $content_divs = [];

	/**
		@brief		IN: The participant object.
		@since		2019-05-04 20:21:46
	**/
	public $participant;
}
