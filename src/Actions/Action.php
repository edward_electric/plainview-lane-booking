<?php

namespace plainview\lane_booking\Actions;

/**
	@brief		Base action.
	@since		2019-03-29 08:41:49
**/
class action
	extends \plainview\pvlb_sdk\wordpress\actions\action
{
	/**
		@brief		Main action prefix.
		@since		2019-03-29 08:41:53
	**/
	public function get_prefix()
	{
		return 'pvlb_';
	}
}
