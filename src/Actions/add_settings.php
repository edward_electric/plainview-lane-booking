<?php

namespace plainview\lane_booking\Actions;

/**
	@brief		Add settings to the settings form.
	@since		2021-05-23 10:52:46
**/
class add_settings
	extends Action
{
	/**
		@brief		IN: The settings Form.
		@since		2021-05-23 10:52:53
	**/
	public $form;

	/**
		@brief		IN: An array of private pages (mostly used for e-mails).
		@since		2021-05-23 10:53:03
	**/
	public $private_pages;
}
