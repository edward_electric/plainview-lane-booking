<?php

namespace plainview\lane_booking\Actions;

/**
	@brief		Before the e-mail is sent to the user.
	@since		2019-04-06 11:42:40
**/
class email_before_send
	extends Action
{
	/**
		@brief		[IN]: Apply CSS styling to the e-mail?
		@since		2019-04-06 11:43:00
	**/
	public $apply_css = true;

	/**
		@brief		IN: The PHPMailer object.
		@since		2019-04-06 11:42:48
	**/
	public $mail;

	/**
		@brief		IN: What type of e-mail this is.
		@since		2019-04-06 11:43:16
	**/
	public $type = false;
}
