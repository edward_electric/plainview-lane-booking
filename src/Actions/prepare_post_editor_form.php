<?php

namespace plainview\lane_booking\Actions;

/**
	@brief		Allow other plugins to modify the post editor form.
	@since		2019-04-27 18:57:16
**/
class prepare_post_editor_form
	extends Action
{
	/**
		@brief		IN/OUT: The form to be modified.
		@since		2019-04-27 18:57:39
	**/
	public $form;

	/**
		@brief		IN: The ID of the post we are editing.
		@since		2019-04-27 18:57:28
	**/
	public $post_id;

	/**
		@brief		IN: The State we are working on.
		@since		2019-04-27 18:59:03
	**/
	public $state;
}
