<?php

namespace plainview\lane_booking\Actions;

/**
	@brief		Save the settings form, saving the values.
	@since		2021-05-23 10:53:32
**/
class save_settings
	extends Action
{
	/**
		@brief		IN: The settings Form.
		@since		2021-05-23 10:52:53
	**/
	public $form;
}
