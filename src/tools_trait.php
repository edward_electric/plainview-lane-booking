<?php

namespace plainview\lane_booking;

/**
	@brief		Handle menu things.
	@since		2019-03-24 18:35:36
**/
trait tools_trait
{
	/**
		@brief		Tools UI.
		@since		2019-04-06 19:37:15
	**/
	public function tools()
	{
		$form = $this->form();
		$form->prefix( 'pvlb' );
		$form->id( 'tools' );
		$r = '';

		$fs = $form->fieldset( 'fs_user_tools' )
			// Fieldset label.
			->label( __( 'User tools', 'pvlb' ) );

		$user_email_input = $fs->email( 'user_email' )
			->description( __( 'E-mail address of user you wish to work with.', 'pvlb' ) )
			// Email address input for tools
			->label( __( 'E-mail', 'pvlb' ) )
			->size( 32 );

		$send_user_imported_email_button = $fs->secondary_button( 'send_user_imported_email' )
			->value( __( 'Send registration e-mail', 'pvlb' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();

			$user_email = $user_email_input->get_filtered_post_value();
			if ( $send_user_imported_email_button->pressed() )
			{
				$user = get_user_by( 'email', $user_email );
				if ( $user )
				{
					$this->send_user_imported_email( $user );
					$r .= $this->info_message_box()->_( __( 'E-mail sent!', 'pvlb' ) );
				}
			}
		}

		$r .= $form->open_tag();
		$r .= $form->display_form_table();
		$r .= $form->close_tag();

		echo $r;
	}
}
