<?php

namespace plainview\lane_booking\Users;

use Exception;

/**
	@brief		A user, ready to be imported.
	@since		2019-03-29 08:12:31
**/
class User
{
	/**
		@brief		The e-mail address.
		@since		2019-03-29 08:12:50
	**/
	public $email;

	/**
		@brief		The username.
		@since		2019-03-29 08:12:43
	**/
	public $username;

	/**
		@brief		Create a user from an array from the csv data.
		@since		2019-03-29 08:02:34
	**/
	public static function from_line_array( $line )
	{
		$r = new static();
		foreach( $line as $key => $value )
			$r->$key = $value;
		$r->email = strtolower( $r->email );
		return $r;
	}

	/**
		@brief		Find the user using first the e-mail address then the username.
		@since		2019-04-22 10:59:58
	**/
	public static function find_user( User $user )
	{
		// Find using email.
		$r = get_user_by( 'email', $user->email );
		if ( ! $r )
			// Try finding user login.
			$r = get_user_by( 'login', $user->username );
		return $r;
	}

	/**
		@brief		Maybe import this user.
		@since		2019-03-29 08:20:05
	**/
	public function maybe_import()
	{
		$wp_user = static::find_user( $this );
		if ( ! $wp_user )
		{
			$wp_user_id = wp_create_user( $this->username, rand( 0, 1000 ) . microtime(), $this->email );
			wp_update_user( [
				'ID' => $wp_user_id,
				'nickname' => $this->username,
				'display_name' => $this->username,
			] );
			$wp_user = get_user_by( 'id', $wp_user_id );
			$action = Plainview_Lane_Booking()->new_action( 'user_imported' );
			$action->user = $this;
			$action->wp_user = $wp_user;
			$action->execute();
		}

		$this->update();
	}

	/**
		@brief		Update the WP User data with our data.
		@since		2019-03-31 09:12:29
	**/
	public function update()
	{
		$wp_user = static::find_user( $this );

		foreach( $this as $key => $value )
		{
			if ( $key == 'email' )
				if ( $this->email != $wp_user->data->user_email )
				{
					// Force a database update of the user, in order to skip the stupid notification.
					global $wpdb;
					$query = sprintf( "UPDATE `%s` SET `user_email` = '%s' WHERE `ID` = '%s'",
						$wpdb->users,
						$this->email,
						$wp_user->data->ID
					);
					$wpdb->query( $query );
				}
			if ( $key == 'username' )
				if ( $this->username != $wp_user->data->display_name )
				{
					wp_update_user( [
						'ID' => $wp_user->data->ID,
						'nickname' => $this->username,
						'display_name' => $this->username,
						'user_login' => $this->username,
					] );
				}
			if ( in_array( $key, [ 'email', 'username' ] ) )
				continue;
			update_user_meta( $wp_user->ID, $key, $value );
		}

		$action = Plainview_Lane_Booking()->new_action( 'user_updated' );
		$action->user = $this;
		$action->wp_user = $wp_user;
		$action->execute();
	}
}
