<?php

namespace plainview\lane_booking\Users;

use Exception;

/**
	@brief		Handles the conversion of csv data to users.
	@since		2019-03-29 07:58:54
**/
class Import
{
	/**
		@brief		Constructor.
		@param		text		$data		Copy pasted from a spreadsheet.
		@since		2019-03-29 07:59:27
	**/
	public function __construct( $data )
	{
		$lines = Plainview_Lane_Booking()->textarea_to_array( $data );

		$headers = array_shift( $lines );
		$headers = explode( "\t", $headers );

		$pass = true;

		if ( count( $headers ) < 2 )
			$pass = false;
		else
		{
			if ( $headers[ 0 ] !== 'email' )
				$pass = false;
			if ( $headers[ 1 ] !== 'username' )
				$pass = false;
		}

		if ( ! $pass )
			throw new Exception( __( 'The first row must contain the following columns: email, username', 'pvlb' ) );

		$this->users = new Users();
		foreach( $lines as $line )
		{
			$line = explode( "\t", $line );
			$line = array_combine( $headers, $line );
			$user = User::from_line_array( $line );
			$this->users->set( $user->email, $user );
		}
	}

	/**
		@brief		Execute the import.
		@since		2019-03-29 08:19:41
	**/
	public function execute()
	{
		foreach( $this->users as $user )
			$user->maybe_import();
	}

	/**
		@brief		Test an import.
		@since		2019-04-22 11:10:31
	**/
	public function test()
	{
		$r = [];
		foreach( $this->users as $user )
		{
			$found_user = $user->find_user( $user );
			if ( $found_user )
				$r []= sprintf( __( 'Line %s / %s, found existing user %s / %s', 'pvlb' ),
					$user->username,
					$user->email,
					$found_user->data->user_email,
					$found_user->data->user_login
				);
			else
				$r []= sprintf( __( 'Line %s / %s, creating user.', 'pvlb' ),
					$user->username,
					$user->email
				);
		}
		return $r;
	}
}
