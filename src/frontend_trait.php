<?php

namespace plainview\lane_booking;

use plainview\lane_booking\States\State;

/**
	@brief		The parts that are shown on the front end.
	@since		2019-03-25 21:00:14
**/
trait frontend_trait
{
	/**
		@brief		Init.
		@since		2019-03-25 21:00:30
	**/
	public function init_frontend_trait()
	{
		$this->add_action( 'tribe_events_single_event_after_the_content', 'display_frontend' );
		$this->add_action( 'tribe_events_single_meta_details_section_end' );
	}

	/**
		@brief		display_frontend
		@since		2019-03-25 21:01:53
	**/
	public function display_frontend( $post_id = null )
	{
		$user_id = $this->user_id();

		// Retrieve the post ID.
		if ( ! $post_id )
		{
			global $post;
			$post_id = $post->ID;
		}

		$state = State::load( $post_id );

		if ( ! $state->is_open() )
		{
			if ( $user_id < 1 )
				return;
			// Is there a timetable?
			if ( ! $state->settings()->get_schedule_ready() )
				return;
			echo $state->groups()->get_schedule_html();
			return;
		}

		// Only display the form if the user is logged in.
		if ( $user_id < 1 )
		{
			$url = $this->current_url();
			$url = wp_login_url( $url );
			echo sprintf( __( 'Please %slog in%s to participate in this event.', 'pvlb' ),
				'<a href="' . $url . '">',
				'</a>'
			);
			return;
		}

		$form = $this->form();
		$form->id( 'plainview_lane_booking' );

		// Show whether the user is a participant or not.
		if ( $state->participants()->is_participant( $user_id ) )
		{
			$not_participating = $form->primary_button( 'not_participating' )
				->value( __( 'I am not coming', 'pvlb' ) );
		}
		else
		{
			if ( ! $state->is_full() )
			{
				$fs = $form->fieldset( 'fs_shooter' )
					// Fieldset label.
					->label( __( 'Participate as shooter', 'pvlb' ) );

				$gun_class_input = $fs->select( 'gun_class' )
					->label( __( 'Gun class', 'pvlb' ) );
				$options = $state->settings()->get( 'gun_classes' );
				// The first option is the default.
				$first = reset( $options );
				$gun_class_input->value( $first );
				// Now we know the default. Sort the items.
				asort( $options );
				foreach( $options as $option )
					$gun_class_input->opt( $option, $option );
				$gun_class_input->autosize();

				if( $state->settings()->get( 'allow_gun_loan' ) )
				{
					$loan_a_gun_input = $fs->checkbox( 'loan_a_gun' )
						->label( __( 'I need to loan a gun', 'pvlb' ) );
				}

				$participate_as_shooter = $fs->primary_button( 'participate_as_shooter' )
					->value( __( 'Sign me up', 'pvlb' ) );
			}

			// Everyone can participate passively.
			$participate_passive = $form->primary_button( 'participate_passive' )
				->value( __( 'Participate as an official', 'pvlb' ) );
		}

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();

			if ( isset( $not_participating ) )
				if ( $not_participating->pressed() )
				{
					$state->participants()->remove_participant();
				}

			if ( isset( $participate_as_shooter ) )
				if ( $participate_as_shooter->pressed() )
				{
					$participant = $state->participants()->add_active();
					$participant->set_gun_class( $gun_class_input->get_filtered_post_value() );
					if ( $state->settings()->get( 'allow_gun_loan' ) )
						if ( $loan_a_gun_input->is_checked() )
							$participant->set_gun_loan( true );
					$state->participants()->sort_all();
				}

			if ( isset( $participate_passive ) )
				if ( $participate_passive->pressed() )
				{
					$state->participants()->add_passive();
				}

			$state->save();
			// Reload the page.
			wp_redirect( remove_query_arg( 'nothing' ) );
		}
		else
			echo $form;
	}

	/**
		@brief		tribe_events_single_meta_details_section_end
		@since		2019-03-26 07:53:48
	**/
	public function tribe_events_single_meta_details_section_end()
	{
		if ( $this->user_id() < 1 )
			return;

		global $post;
		$post_id = $post->ID;

		$state = State::load( $post_id );

		$all_count = $state->participants()->all()->count();

		$r = '<div class="pvlb">';

		if ( $state->is_open() )
		{
			$show_time = $this->get_local_option( 'show_timeout' );
			if ( $show_time != '' )
			{
				$time = $state->get_timeout_time();
				$ago = human_time_diff( $time );
				$time = $state->get_time_offset( $time );
				$time_string = date_i18n( get_option( 'date_format' ) . ' ' . get_option( 'time_format' ), $time );
				$r .= '<dt class="last_signup">' . __( 'Last signup', 'pvlb' ) . '</dt>';
				if ( $show_time == 'human_time' )
					$time_to_show = sprintf( '<span title="%s">%s</span>', $time_string, $ago );
				if ( $show_time == 'time' )
					$time_to_show = $time_string;
				$r .= '<dd class="last_signup">' . $time_to_show . '</dt>';
			}
		}

		// Only display participants if
		if ( $all_count > 0 )
			if ( ! $state->settings()->get_schedule_ready() )
			{
				$count = $state->participants()->active()->count();

				$r .= '<dt class="participants">' . __( 'Participants', 'pvlb' ) . sprintf( ' <span class="counter"><span class="separator">(</span>%s<span class="separator">)</span></span>', $count ) . '</dt>';
				$participants = [];
				foreach( $state->participants()->all() as $participant_id => $participant )
				{
					// Build the CSS classes.
					$participant_classes = [];
					$participant_classes []= 'gun_class_' . $participant->get( 'gun_class' );
					if ( $participant->is_loaning() )
						$participant_classes []= 'loaning';
					if ( $state->participants()->is_active( $participant_id ) )
						$participant_classes []= 'active';
					else
						$participant_classes []= 'passive';

					$participants [ $participant->get( 'name' ) ]= sprintf( '<div class="participant %s"><span class="participant">%s</span><span class="gun_class">%s</span></div>',
						implode( ' ', $participant_classes ),
						$participant->get( 'name' ),
						$participant->get( 'gun_class' )
					);
				}
				// Sorting by name (key) requires us to flip the key value.
				$participants = array_flip( $participants );
				natcasesort( $participants );
				$participants = array_flip( $participants );
				$r .= '<dd class="participants">' . implode( '', $participants ) . '</dt>';
			}

		$r .= '</div>';		// pvlb

		$this->wp_enqueue_scripts();

		echo $r;
	}
}
