<?php

namespace plainview\lane_booking\States;

/**
	@brief		Contains the state of a lane booking.
	@since		2019-03-25 20:51:52
**/
class State
	extends \plainview\pvlb_sdk\collections\collection
{
	/**
		@brief		Delete the state from this post, if found.
		@since		2019-03-31 11:48:56
	**/
	public static function delete( $post_id )
	{
		delete_post_meta( $post_id, static::postmeta_key() );
	}

	/**
		@brief		Send the schedule e-mail to people.
		@since		2019-04-06 11:02:50
	**/
	public function email_schedule( $emails )
	{
		$pvlb = Plainview_Lane_Booking();
	}

	/**
		@brief		Return the event's name.
		@since		2019-04-06 11:20:42
	**/
	public function get_event_name()
	{
		$post = get_post( $this->post_id );
		return $post->post_title;
	}

	/**
		@brief		Convenience method to return the unix time of the event.
		@since		2019-04-06 11:22:25
	**/
	public function get_event_time()
	{
		return static::get_time( $this->post_id );
	}

	/**
		@brief		Return how many groups are necessary to fit all of the users.
		@since		2019-03-31 22:06:46
	**/
	public function get_group_count()
	{
		if ( $this->settings()->lane_count() < 1 )
			return 0;
		return ceil( $this->participants()->active()->count() / $this->settings()->lane_count() );
	}

	/**
		@brief		Return the time for this event.
		@since		2019-03-25 21:43:42
	**/
	public static function get_time( $post_id )
	{
		$time = get_post_meta( $post_id, '_EventStartDate', true );
		$time = strtotime( $time );
		if ( ! $time )	// No value at all? Probably a new post.
			$time = time();
		$diff = static::get_time_offset( $time );
		$diff = $diff - $time;
		$time -= $diff;
		return $time;
	}

	/**
		@brief		Offset this unix time according to the blog settings.
		@since		2019-03-25 21:44:09
	**/
	public static function get_time_offset( $time )
	{
		$gmt_offset = get_option( 'gmt_offset' );
		return $time + ( $gmt_offset * HOUR_IN_SECONDS );
	}

	/**
		@brief		Return the unix time when the signup closes.
		@since		2019-04-22 17:30:08
	**/
	public function get_timeout_time()
	{
		// And check that the time is good.
		$timeout = $this->settings()->get( 'signup_hours' );
		$timeout *= 60 * 60;
		return ( static::get_time(  $this->post_id  ) - $timeout );
	}

	/**
		@brief		Return the groups collection.
		@since		2019-03-25 21:44:39
	**/
	public function groups()
	{
		if ( ! isset( $this->groups ) )
			$this->groups = new Groups();

		// Make more groups if necessary.
		while( count( $this->groups ) < $this->get_group_count() )
		{
			$group = new Group();
			$group->set_parent( $this->groups );
			$this->groups->set( count( $this->groups ) + 1, $group );
		}

		// Remove groups no longer necessary.
		while( count( $this->groups ) > $this->get_group_count() )
		{
			$last = array_keys( $this->groups->to_array() );
			$last = end( $last );
			$this->groups->forget( $last );
		}

		return $this->groups;
	}

	/**
		@brief		Does the state allow for more shooters?
		@since		2019-03-25 21:59:46
	**/
	public function is_full()
	{
		$lane_count = $this->settings()->lane_count();
		$active_participants = $this->participants()->active()->count();
		$max_groups = $this->settings()->get( 'max_groups' );
		return $active_participants >= ( $lane_count * $max_groups );
	}

	/**
		@brief		Are people still allowed to sign up?
		@since		2019-03-25 21:41:24
	**/
	public function is_open()
	{
		// Check that this is enabled.
		if ( ! $this->settings()->is_enabled() )
			return false;
		$timeout_time = $this->get_timeout_time();
		return time() < $timeout_time;
	}

	/**
		@brief		Convert the whole state to a json object.
		@since		2019-03-31 21:41:27
	**/
	public function json_everything()
	{
		$r = (object) [];
		$r->groups = $this->groups()->to_array();
		$r->participants = $this->participants()->to_array();
		$r->settings = $this->settings()->to_array();
		$r->settings[ 'group_count' ] = $this->get_group_count();
		return json_encode( $r );
	}

	/**
		@brief		Load from this post ID.
		@since		2019-03-25 20:56:54
	**/
	public static function load( $post_id )
	{
		$data = get_post_meta( $post_id, static::postmeta_key(), true );
		$data = base64_decode( $data );
		$data = unserialize( $data );
		if ( ! is_object( $data ) )
		{
			$data = new static();
			$data->post_id = $post_id;
		}

		// Create the components.
		$data->groups()->set_parent( $data );
		$data->participants()->set_parent( $data );
		$data->settings()->set_parent( $data );

		$pvlb = Plainview_Lane_Booking();
		$keys = [
			'gun_classes',
			'lane_numbers',
			'max_groups',
			'signup_hours',
			'allow_gun_loan',
		];
		foreach( $keys as $key )
			if ( ! $data->settings()->has( $key ) )
				$data->settings()->set( $key, $pvlb->get_local_option( $key ) );

		return $data;
	}

	/**
		@brief		Return the participants collection.
		@since		2019-03-25 21:44:39
	**/
	public function participants()
	{
		if ( ! isset( $this->participants ) )
			$this->participants = new Participants();
		return $this->participants;
	}

	/**
		@brief		postmeta_key
		@since		2019-03-25 21:07:08
	**/
	public static function postmeta_key()
	{
		return 'plainview_lane_booking_state';
	}

	/**
		@brief		Save us to the postmeta.
		@since		2019-03-25 20:56:14
	**/
	public function save()
	{
		$data = serialize( $this );
		$data = base64_encode( $data );
		update_post_meta( $this->post_id, static::postmeta_key(), $data );
		return $this;
	}

	/**
		@brief		Return the settings collection.
		@since		2019-03-25 21:44:39
	**/
	public function settings()
	{
		if ( ! isset( $this->settings ) )
			$this->settings = new Settings();
		return $this->settings;
	}

	/**
		@brief		Is the event too old to have a state?
		@since		2019-03-31 11:36:34
	**/
	public static function too_old( $post_id )
	{
		return ( time() - static::get_time( $post_id ) > ( WEEK_IN_SECONDS ) );
	}
}
