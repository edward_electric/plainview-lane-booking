<?php

namespace plainview\lane_booking\States;

/**
	@brief		Container for grouping.
	@since		2019-03-31 22:33:30
**/
class Groups
	extends \plainview\pvlb_sdk\collections\collection
{
	use parent_trait;

	/**
		@brief		Get the schedule as an HTML table.
		@since		2019-04-04 22:31:01
	**/
	public function get_schedule_html()
	{
		$r = '<div class="plainview_lane_booking_schedule_html">';

		foreach( $this as $group_id => $group )
		{
			if ( ! $group->has_participants() )
				continue;

			$r .= sprintf( '<div class="group group_%s">', $group_id );

			$table = Plainview_Lane_Booking()->table();
			$table->caption()->textf( __( 'Group %d', 'pvlb' ), $group_id );

			$head = $table->head();
			$row = $head->row();
			$th = $row->th( 'participant_name' )->text(  __( 'Participant', 'pvlb' ) );
			$th = $row->th( 'lane_id' )->text(  __( 'Lane', 'pvlb' ) );
			$th = $row->th( 'gun_class' )->text(  __( 'Gun class', 'pvlb' ) );

			$body = $table->body();
          
          	$active = $group->active();
         	if ( $active )
            {
				$lanes_names = $group->as_lane_and_name( $active );
              foreach( $lanes_names as $lane_id => $participant )
              {
                  $row = $body->row();
                  $td = $row->td( 'participant_name' )->text( $participant->get( 'name' ) );
                  $td = $row->td( 'lane_id' )->text( $lane_id );
                  $td = $row->td( 'gun_class' )->text( $participant->get( 'gun_class' ) );
              }
            }
          

			$r .= $table;

          	$passive = $group->passive();
         	if ( $passive )
            {
              $lanes_names = $group->as_lane_and_name( $passive );
              if ( count( $lanes_names ) > 0 )
              {
                  $names = [];
                  foreach( $lanes_names as $participant )
                      $names [] = $participant->get( 'name' );
                  $r .= sprintf( '<p>%s: %s</p>',
                      __( 'Officials', 'pvlb' ),
                      implode( ', ', $names )
                  );
              }
            }

			$r .= '</div>';
		}
		$r .= '</div>';

		return $r;
	}

	/**
		@brief		Convenience method to return a group.
		@since		2019-04-04 14:25:06
	**/
	public function group( $group_id )
	{
		if ( ! $this->has( $group_id ) )
		{
			$group = new Group();
			$group->set_parent( $this );
			$this->set( $group_id, $group );
		}

		return $this->get( $group_id );
	}

	/**
		@brief		Do any of the groups have any participants?
		@since		2019-04-04 22:29:09
	**/
	public function has_participants()
	{
		foreach( $this as $group )
			if ( $group->has_participants() )
				return true;
		return false;
	}

	/**
		@brief		Initialize everything needed in the groups.
		@since		2019-04-04 14:31:33
	**/
	public function init()
	{
		foreach( $this as $group_id => $group )
			$group->init();
		return $this;
	}
}
