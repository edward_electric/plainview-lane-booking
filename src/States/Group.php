<?php

namespace plainview\lane_booking\States;

/**
	@brief		The group containint lane assignments.
	@since		2019-03-31 22:38:16
**/
class Group
	extends \plainview\pvlb_sdk\collections\collection
{
	use parent_trait;

	/**
		@brief		Return the active lanes.
		@since		2019-04-04 14:19:51
	**/
	public function active()
	{
		return $this->collection( 'active' );
	}

	/**
		@brief		Return an array of lane_id => participant_name;
		@since		2019-04-04 22:39:27
	**/
	public function as_lane_and_name( $collection )
	{
		$r = new \plainview\pvlb_sdk\collections\collection();
		$participants = $this->get_parent()->get_parent()->participants()->all();
		foreach( $collection as $lane_id => $participant_id )
		{
			$participant = $participants->get( $participant_id );
          if ( ! $participant )
            continue;
			$r->set( $lane_id, $participant );
		}
		return $r->sort_by( function( $participant )
		{
			return $participant->get( 'name' );
		} );
	}

	/**
		@brief		Does this group have any participants?
		@since		2019-04-04 22:29:41
	**/
	public function has_participants()
	{
		if ( $this->active()->count() > 0 )
			return true;
		if ( $this->passive()->count() > 0 )
			return true;
		return false;
	}

	/**
		@brief		Initialize this group.
		@since		2019-04-04 14:33:12
	**/
	public function init()
	{
		$this->active();
		$this->passive();
		return $this;
	}

	/**
		@brief		Return the passive lanes.
		@since		2019-04-04 14:19:51
	**/
	public function passive()
	{
		return $this->collection( 'passive' );
	}
}
