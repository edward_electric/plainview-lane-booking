<?php

namespace plainview\lane_booking\States;

/**
	@brief		Handle the setting and getting of the parent object.
	@since		2019-03-31 22:38:48
**/
trait parent_trait
{
	/**
		@brief		Return the parent.
		@since		2019-03-31 22:39:28
	**/
	public function get_parent()
	{
		return $this->__parent;
	}

	/**
		@brief		Set the parent class.
		@since		2019-03-31 22:39:15
	**/
	public function set_parent( $parent )
	{
		$this->__parent = $parent;
	}
}