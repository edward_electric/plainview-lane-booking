<?php

namespace plainview\lane_booking\States;

/**
	@brief		Someone participating in this event.
	@since		2019-03-25 21:49:53
**/
class Participant
	extends \plainview\pvlb_sdk\collections\collection
{
	/**
		@brief		Is this user loaning a gun?
		@since		2019-03-26 08:08:42
	**/
	public function is_loaning()
	{
		return $this->get( 'gun_loan' ) == true;
	}

	/**
		@brief		Return the gun class this user wants to shoot.
		@since		2019-03-26 08:08:20
	**/
	public function get_gun_class()
	{
		return $this->get( 'gun_class' );
	}

	/**
		@brief		Return the user meta value for this key.
		@details	Convenience method.
		@since		2019-05-04 20:26:39
	**/
	public function get_meta( $key, $default = false )
	{
		return get_user_meta( $this->get( 'id' ), $key, $default );
	}

	/**
		@brief		Set the gun class this user is shooting.
		@since		2019-03-26 08:07:48
	**/
	public function set_gun_class( $gun_class )
	{
		$this->set( 'gun_class', $gun_class );
		return $this;
	}

	/**
		@brief		Set the gun loan status.
		@since		2019-03-26 08:09:12
	**/
	public function set_gun_loan( $loaning = true )
	{
		if ( ! $loaning )
			$this->forget( 'gun_loan' );
		else
			$this->set( 'gun_loan', true );
		return $this;
	}

	/**
		@brief		Convert this participant to an html container used in the booking editor.
		@since		2019-04-02 21:02:44
	**/
	public function to_container_html()
	{
		$id = $this->get( 'id' );

		$container = new \plainview\pvlb_sdk\html\div();
		$container->css_class( 'participant' );
		$container->css_class( 'container' );
		$container->css_class( 'unassigned' );
		$container->css_class( 'participant_' . $id );
		$container->append_attribute( 'data-id', $id );

		$name = new \plainview\pvlb_sdk\html\div();
		$name->css_class( 'name' );
		$name->content( $this->get( 'name' ) );

		if ( $this->has( 'gun_loan' ) )
			$container->css_class( 'gun_loan' );

		$gun_class = $this->get( 'gun_class' );
		if ( $gun_class != '' )
		{
			$gun_class = new \plainview\pvlb_sdk\html\div();
			$gun_class->css_class( 'gun_class' );
			$gun_class->content( $this->get( 'gun_class' ) );
		}

		if ( ! $this->has( 'gun_class' ) )
			$container->css_class( 'passive' );
		else
			$container->css_class( 'active' );

		$action = plainview_lane_booking()->new_action( 'participant_to_container_html' );
		$action->container = $container;
		$action->content_divs = [
			'name' => $name,
			'gun_class' => $gun_class,
		];
		$action->participant = $this;
		$action->execute();

		$content = '';
		foreach( $action->content_divs as $content_div )
			if ( $content_div )
				$content .= $content_div->toString();

		$container->content( $content );
		return $container->toString();
	}
}
