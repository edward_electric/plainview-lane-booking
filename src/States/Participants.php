<?php

namespace plainview\lane_booking\States;

/**
	@brief		Helper collection for just participants.
	@since		2019-03-25 21:56:24
**/
class Participants
	extends \plainview\pvlb_sdk\collections\collection
{
	use parent_trait;

	/**
		@brief		Active participants are the ones doing the shooting.
		@since		2019-03-25 21:56:55
	**/
	public function active()
	{
		return $this->collection( 'active' );
	}

	/**
		@brief		Convenience method to add this user as an active participant.
		@since		2019-03-26 07:31:07
	**/
	public function add_active( $participant = null )
	{
		if ( ! $participant )
			$participant = $this->participant();
		$this->all()->set( $participant->get( 'id' ), $participant );
		$this->active()->set( $participant->get( 'id' ), $participant );
		$this->sort_all();
		return $participant;
	}

	/**
		@brief		Convenience method to add this user as a passive participant.
		@since		2019-03-26 07:31:07
	**/
	public function add_passive( $participant = null )
	{
		if ( ! $participant )
			$participant = $this->participant();
		$participant = $this->participant();
		$this->all()->set( $participant->get( 'id' ), $participant );
		$this->passive()->set( $participant->get( 'id' ), $participant );
		$this->sort_all();
		return $participant;
	}

	/**
		@brief		Return the collection of ALL users.
		@since		2019-03-31 21:54:45
	**/
	public function all()
	{
		return $this->collection( 'all' );
	}

	/**
		@brief		Is this user an active participant?
		@since		2019-03-29 17:42:33
	**/
	public function is_active( $user_id )
	{
		return $this->active()->has( $user_id );
	}

	/**
		@brief		Is this user an passive participant?
		@since		2019-03-29 17:42:33
	**/
	public function is_passive( $user_id )
	{
		return $this->passive()->has( $user_id );
	}

	/**
		@brief		Override the has to check both the active and passive collections.
		@since		2019-03-26 07:33:16
	**/
	public function is_participant( $user_id )
	{
		return $this->active()->has( $user_id ) || $this->passive()->has( $user_id );
	}

	/**
		@brief		Create a new participant.
		@since		2019-03-26 07:32:03
	**/
	public function participant( $user_id = null )
	{
		if ( ! $user_id )
			$user_id = Plainview_Lane_Booking()->user_id();
		$user = get_user_by( 'id', $user_id );
		$participant = new Participant();
		$participant->set( 'name', $user->data->display_name );
		$participant->set( 'created_at', time() );
		$participant->set( 'id', $user_id );
		return $participant;
	}

	/**
		@brief		Passive participants are officials and spectators.
		@since		2019-03-25 21:57:10
	**/
	public function passive()
	{
		return $this->collection( 'passive' );
	}

	/**
		@brief		Convenience method to remove this participant.
		@since		2019-03-26 07:41:07
	**/
	public function remove_participant( $user_id = null )
	{
		if ( ! $user_id )
			$user_id = Plainview_Lane_Booking()->user_id();
		$this->all()->forget( $user_id );
		$this->active()->forget( $user_id );
		$this->passive()->forget( $user_id );
	}

	/**
		@brief		Sort all of the participants.
		@since		2019-03-31 21:58:08
	**/
	public function sort_all()
	{
		$this->active()->sortBy( function( $participant )
		{
			// Loaners first.
			return sprintf( '%d-%s', $participant->is_loaning(), $participant->get( 'name' ) );
		} );
		$this->all()->sortBy( function( $participant )
		{
			$gun_class = ( $participant->get_gun_class() != '' );
			// Active loaners, active, passive.
			$key = sprintf( '%d-%s-%s',
				( ! $participant->is_loaning() ),		// ! in order to get the loaners first.
				( ! $gun_class ),				// To get the passive last
				$participant->get( 'name' )
			);
			return $key;
		} );
		$this->passive()->sortBy( function( $participant )
		{
			return $participant->get( 'name' );
		} );
	}
}
