<?php

namespace plainview\lane_booking\States;

/**
	@brief		Helper collection for just settings.
	@since		2019-03-25 21:56:24
**/
class Settings
	extends \plainview\pvlb_sdk\collections\collection
{
	use parent_trait;

	/**
		@brief		Is the schedule ready?
		@since		2019-04-22 13:02:16
	**/
	public function get_schedule_ready()
	{
		return $this->get( 'schedule_ready', false );
	}

	/**
		@brief		Set the enabled state.
		@since		2019-03-31 10:48:19
	**/
	public function enabled( $enabled )
	{
		$this->set( 'enabled', $enabled );
		return $this;
	}

	/**
		@brief		Is this state enabled?
		@since		2019-03-31 10:47:22
	**/
	public function is_enabled()
	{
		return $this->get( 'enabled', false );
	}

	/**
		@brief		Return the number of lanes available.
		@since		2019-03-25 22:00:44
	**/
	public function lane_count()
	{
		return count( array_filter( $this->get( 'lane_numbers', [] ) ) );
	}

	/**
		@brief		Set the ready status of the schedule.
		@since		2019-04-22 13:02:54
	**/
	public function set_schedule_ready( $ready )
	{
		$this->set( 'schedule_ready', $ready );
		return $this;
	}
}
