<?php

namespace plainview\lane_booking;

use Exception;

/**
	@brief		User handling.
	@since		2019-03-26 07:04:17
**/
trait user_trait
{
	/**
		@brief		Init.
		@since		2019-03-26 07:04:29
	**/
	public function init_user_trait()
	{
		$this->add_action( 'personal_options_update', 'edit_user_profile_update' );
		$this->add_action( 'pvlb_user_imported' );
	}

	/**
		@brief		A user was imported.
		@since		2019-03-31 09:23:24
	**/
	public function pvlb_user_imported( $action )
	{
		if ( $action->is_finished() )
			return;
		$this->send_user_imported_email( $action->wp_user );
	}

	/**
		@brief		Interface for importing users.
		@since		2019-03-29 07:39:12
	**/
	public function users_import()
	{
		$form = $this->form();
		$form->id( 'users_import' );
		$r = '';

		$m = $form->markup( 'm_csv' )
			->value( __( 'Use the text area below to import users from a spreadsheet. The mandatory columns are: e-mail, username', 'pvlb' ) );

		$csv_input = $form->textarea( 'csv' )
			->description( __( 'Data pasted from the spreadsheet.', 'pvlb' ) )
			->label( __( 'Spreadsheet data', 'pvlb' ) )
			->rows( 20, 40 );

		$m = $form->markup( 'm_single' )
			->value( __( 'Use the inputs below to create a single user.', 'pvlb' ) );

		$email_input = $form->email( 'user_email' )
			->description( __( 'The e-mail address of the user to create.', 'pvlb' ) )
			->label( __( 'E-mail address', 'pvlb' ) )
			->lowercase()
			->size( 32 )
			->trim();

		$username_input = $form->text( 'user_username' )
			->description( __( 'The user name of the user to create.', 'pvlb' ) )
			->label( __( 'User name', 'pvlb' ) )
			->size( 32 )
			->trim();

		$test_button = $form->primary_button( 'test' )
			// Button text
			->value( __( 'Test import', 'pvlb' ) );

		$import_button = $form->secondary_button( 'import' )
			// Button text
			->value( __( 'Import', 'pvlb' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();

			try
			{
				$user_email = $email_input->get_filtered_post_value();
				$user_username = $username_input->get_filtered_post_value();

				if ( ( $user_email != '' ) && ( $user_username != '' ) )
					$csv = sprintf( "email\tusername\n%s\t%s", $user_email, $user_username );
				else
					$csv = $csv_input->get_filtered_post_value();

				$user_import = new Users\Import( $csv );
				if ( $test_button->pressed() )
				{
					$text = $user_import->test();
					$text = implode( '<br/>', $text );
					$r .= $this->info_message_box()->_( __( 'Ready to import: %s', 'pvlb' ), $text );
				}
				if ( $import_button->pressed() )
				{
					$user_import->execute();
					$r .= $this->info_message_box()->_( __( 'Import is complete.', 'pvlb' ), count( $user_import->users ) );
				}
			}
			catch ( Exception $e )
			{
				$r .= $this->error_message_box()->_( __( 'Error during import: %s', 'pvlb' ), $e->getMessage() );
			}
		}

		$r .= $form->open_tag();
		$r .= $form->display_form_table();
		$r .= $form->close_tag();

		echo $r;
	}
}
