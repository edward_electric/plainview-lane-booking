<?php

namespace plainview\lane_booking;

/**
	@brief		Misc functions.
	@since		2019-03-24 20:36:32
**/
trait misc_trait
{
	/**
		@brief		init_misc_trait
		@since		2019-03-26 07:49:39
	**/
	public function init_misc_trait()
	{
	}

	/**
		@brief		The site options we store.
		@since		2018-01-25 21:07:47
	**/
	public function local_options()
	{
		return array_merge( [
			/**
				@brief		The e-mail address from which to send e-mails.
				@since		2019-03-25 12:08:56
			**/
			'email_address' => 'none@example.org',
			/**
				@brief		The name of the e-mail address from which to send e-mails.
				@since		2019-03-25 12:09:17
			**/
			'email_name' => 'Plainview Lane Booking',
			/**
				@brief		Page to use as the template for the booking complete page with the timetable on it.
				@since		2019-03-29 19:24:15
			**/
			'email_schedule_page_id' => 0,
			/**
				@brief		Page to use to send to newly-imported users.
				@since		2019-03-29 19:23:37
			**/
			'email_user_imported_page_id' => 0,
			/**
				@brief		Which gun classes are available.
				@details	Note that the first line is used as the default value, but the values will be displayed alphabetically.
				@since		2019-03-25 11:15:40
			**/
			'gun_classes' => [
				'C',
				'A',
				'B',
			],
			/**
				@brief		Which lanes are available for use.
				@since		2019-03-25 11:03:49
			**/
			'lane_numbers' => [ '1', '2', '3', '4', '5', '6', '7', '8', '9', '10' ],
			/**
				@brief		Maximum amount of groups shooters are placed into. This value decides how many groups to make, depending on available lanes.
				@since		2019-03-25 11:04:47
			**/
			'max_groups' => 2,
			/**
				@brief		Show the signup timeout on the front-end.
				@details	Default is 'time', 'human_time', ''
				@since		2019-04-22 17:42:52
			**/
			'show_timeout' => 'human_time',
			/**
				@brief		How many hours before the signup closes.
				@since		2019-03-25 11:58:23
			**/
			'signup_hours' => 48,
			/**
			@brief		Is gun loaning allowed
			@since		2019-05-01 17:41:00
			 **/
			'allow_gun_loan' => true,
		], parent::local_options() );
	}

	/**
		@brief		Generate a new action.
		@details	Convenience method so that other plugins don't have to use the whole namespace for the class' actions.
		@since		2019-03-29 08:41:23
	**/
	public function new_action( $action_name )
	{
		$called_class = get_called_class();
		// Strip off the class name.
		$namespace = preg_replace( '/(.*)\\\\.*/', '\1', $called_class );
		$classname = $namespace  . '\\Actions\\' . $action_name;
		return new $classname();
	}

	/**
		@brief		Replace the shortcodes in this text.
		@since		2019-03-31 09:18:28
	**/
	public function replace_shortcodes( $text, $shortcodes )
	{
		foreach( $shortcodes as $shortcode => $value )
		{
			$str = '[' . $shortcode . ']';
			$text = str_replace( $str, $value, $text );
		}
		return $text;
	}

	/**
		@brief		Enqueue JS and CSS.
		@since		2019-04-24 21:29:28
	**/
	public function wp_enqueue_scripts()
	{
		wp_enqueue_script( 'plainview_lane_booking', $this->paths[ 'url' ] . '/js/js.js', '', $this->plugin_version, [ 'jquery' ] );
		wp_enqueue_style( 'plainview_lane_booking', $this->paths[ 'url' ] . '/css/css.css', '', $this->plugin_version );
	}
}
