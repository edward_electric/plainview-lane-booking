<?php

namespace plainview\lane_booking;

/**
	@brief		Handle settings.
	@since		2019-03-24 18:55:09
**/
trait settings_trait
{
	/**
		@brief		Modify the form with the event settings.
		@since		2019-03-31 11:11:08
	**/
	public function add_form_event_settings( $form, $values )
	{
		$signup_hours_input = $form->number( 'signup_hours' )
			->description( __( 'How many hours before the event the possibility to sign up closes.', 'pvlb' ) )
			->label( __( 'Hours before signup closes', 'pvlb' ) )
			->min( 1, 8760 )	// 1 year
			->value( $values[ 'signup_hours' ] );

		$lane_numbers = implode( ' ', $values[ 'lane_numbers' ] );
		$lane_numbers_input = $form->text( 'lane_numbers' )
			->description( __( 'A list of lane numbers, with spaces between, that are available for use.', 'pvlb' ) )
			->label( __( 'Available lane numbers', 'pvlb' ) )
			->size( 32 )
			->trim()
			->value( $lane_numbers );

		$max_groups_input = $form->number( 'max_groups' )
			->description( __( 'The maximum amount of groups shooters are placed into. One group = all lanes full.', 'pvlb' ) )
			->label( __( 'Maximum amount of groups', 'pvlb' ) )
			->min( 1, 20 )
			->value( $values[ 'max_groups' ] );

		$gun_classes = implode( "\n", $values[ 'gun_classes' ] );
		$gun_classes_input = $form->textarea( 'gun_classes' )
			->description( __( 'Which gun classes may be used or loaned. Specify one class per line. The lines will be displayed to the user alphabetically. The first line is marked as the default value.', 'pvlb' ) )
			->label( __( 'Gun classes', 'pvlb' ) )
			->rows( 5 )
			->value( $gun_classes );

		$allow_gun_loan_input = $form->checkbox( 'allow_gun_loan' )
			->checked( $values[ 'allow_gun_loan' ] )
			->description( __( 'Should shooters be allowed to loan a gun for this event.', 'pvlb' ) )
			->label( __( 'Allow gun loaning', 'pvlb' ) )
			->value( $values[ 'allow_gun_loan' ] );
	}

	/**
		@brief		Return an array of the settings keys used in events.
		@since		2019-03-31 11:26:34
	**/
	public static function get_event_settings_keys()
	{
		return [
			'allow_gun_loan',
			'lane_numbers',
			'max_groups',
			'gun_classes',
			'signup_hours',
		];
	}

	/**
		@brief		Retrieve the values from the form.
		@since		2019-03-31 11:17:20
	**/
	public function get_form_event_settings( $form )
	{
		$values = [];
		foreach( static::get_event_settings_keys() as $key )
			$values[ $key ] = $form->input( $key )->get_post_value();
		return $values;
	}

	/**
		@brief		Settings
		@since		2019-03-24 18:55:23
	**/
	public function settings()
	{
		$form = $this->form();
		$form->prefix( 'pvlb' );
		$form->id( 'settings' );
		$r = '';

		$fs = $form->fieldset( 'fs_defaults' )
			// Fieldset label.
			->label( __( 'Defaults', 'pvlb' ) );

		$m = $fs->markup( 'm_defaults' )
			->value( __( 'These settings are used as the defaults when a lane booking is created for an event.', 'pvlb' ) );

		$this->add_form_event_settings( $fs, [
			'allow_gun_loan' => $this->get_local_option( 'allow_gun_loan' ),
			'signup_hours' => $this->get_local_option( 'signup_hours' ),
			'lane_numbers' => $this->get_local_option( 'lane_numbers' ),
			'max_groups' => $this->get_local_option( 'max_groups' ),
			'gun_classes' => $this->get_local_option( 'gun_classes' ),
		] );

		$fs = $form->fieldset( 'fs_email' )
			// Fieldset label.
			->label( __( 'E-mail settings', 'pvlb' ) );

		$email_address_input = $form->email( 'email_address' )
			->description( __( 'Which e-mail address to use when sending e-mail.', 'pvlb' ) )
			->label( __( 'E-mail address', 'pvlb' ) )
			->size( 32 )
			->value( $this->get_local_option( 'email_address' ) );

		$email_name_input = $form->text( 'email_name' )
			->description( __( 'Which name to use when sending e-mail.', 'pvlb' ) )
			->label( __( 'E-mail name', 'pvlb' ) )
			->size( 32 )
			->value( $this->get_local_option( 'email_name' ) );

		$private_pages = get_posts( [
			'post_type' => 'page',
			'post_status' => 'private',
			'numberposts' => -1,
			'order' => 'ASC',
			'orderby' => 'name',
		] );

		$email_schedule_page_id_input = $form->select( 'email_schedule_page_id' )
			->description( __( 'Which page to use as the template when the booking process is complete.', 'pvlb' ) )
			->label( __( 'Booking complete template', 'pvlb' ) )
			->opt( '0', __( 'No page selected', 'pvlb' ) )
			->opt( '-1', __( 'Create a new page', 'pvlb' ) )
			->value( $this->get_local_option( 'email_schedule_page_id' ) );
		foreach( $private_pages as $page )
			$email_schedule_page_id_input->opt( $page->ID, $page->post_title );

		$email_user_imported_page_id_input = $form->select( 'email_user_imported_page_id' )
			->description( __( 'Which page to use as the template for new users.', 'pvlb' ) )
			->label( __( 'User imported template', 'pvlb' ) )
			->opt( '0', __( 'No page selected', 'pvlb' ) )
			->opt( '-1', __( 'Create a new page', 'pvlb' ) )
			->value( $this->get_local_option( 'email_user_imported_page_id' ) );
		foreach( $private_pages as $page )
			$email_user_imported_page_id_input->opt( $page->ID, $page->post_title );

		$fs = $form->fieldset( 'fs_misc' )
			// Fieldset label.
			->label( __( 'Misc settings', 'pvlb' ) );

		$show_timeout_input = $form->select( 'show_timeout' )
			->description( __( 'Show visitors the last signup time.', 'pvlb' ) )
			->label( __( 'Show signup timeout', 'pvlb' ) )
			->opt( '', __( 'No', 'pvlb' ) )
			->opt( 'human_time', __( 'Show human time', 'pvlb' ) )
			->opt( 'time', __( 'Show real time', 'pvlb' ) )
			->value( $this->get_local_option( 'show_timeout' ) );

		$this->add_debug_settings_to_form( $form );

		$action = $this->new_action ( 'add_settings' );
		$action->form = $form;
		$action->private_pages = $private_pages;
		$action->execute();

		$form->primary_button( 'save' )
			->value( __( 'Save settings', 'pvlb' ) );

		if ( $form->is_posting() )
		{
			$form->post();
			$form->use_post_values();

			$values = $this->get_form_event_settings( $form );
			$this->update_local_option( 'allow_gun_loan', $values[ 'allow_gun_loan' ] );
			$gun_classes = $this->textarea_to_array( $values[ 'gun_classes' ] );
			$this->update_local_option( 'gun_classes', $gun_classes );
			$lane_numbers = explode( ' ', $values[ 'lane_numbers' ] );
			$this->update_local_option( 'lane_numbers', $lane_numbers );
			$this->update_local_option( 'max_groups', $values[ 'max_groups' ] );
			$this->update_local_option( 'signup_hours', $values[ 'signup_hours' ] );

			$this->update_local_option( 'email_address', $email_address_input->get_post_value() );
			$this->update_local_option( 'email_name', $email_name_input->get_post_value() );

			$page_id = $email_schedule_page_id_input->get_post_value();
			if ( $page_id == -1 )
				$page_id = $this->create_booking_done_email_page();
			$this->update_local_option( 'email_schedule_page_id', $page_id );

			$page_id = $email_user_imported_page_id_input->get_post_value();
			if ( $page_id == -1 )
				$page_id = $this->create_user_imported_email_page();
			$this->update_local_option( 'email_user_imported_page_id', $page_id );

			$value = $show_timeout_input->get_filtered_post_value();
			$this->update_local_option( 'show_timeout', $value );

			$this->save_debug_settings_from_form( $form );

			$action = $this->new_action ( 'save_settings' );
			$action->form = $form;
			$action->private_pages = $private_pages;
			$action->execute();

			$r = $this->info_message_box()->_( __( 'Settings saved!', 'pvlb' ) );

			echo $r;

			$_POST = [];
			$function = __FUNCTION__;
			echo $this->$function();
			return;
		}

		$r .= $form->open_tag();
		$r .= $form->display_form_table();
		$r .= $form->close_tag();

		echo $r;
	}
}
