<?php

namespace plainview\lane_booking;

use plainview\lane_booking\States\State;

/**
	@brief		Things related to the post editor.
	@since		2019-03-24 20:41:12
**/
trait post_editor_trait
{
	/**
		@brief		Init this trait.
		@since		2019-03-24 20:41:18
	**/
	public function init_post_editor_trait()
	{
		$this->add_action( 'add_meta_boxes' );
		$this->add_action( 'save_post' );
	}

	/**
		@brief		Add the meta box.
		@since		2019-03-24 20:43:01
	**/
	public function add_meta_boxes()
	{
		$post_types = [ 'tribe_events' ];
		$post_types = apply_filters( 'plainview_lane_booking_post_types', $post_types );
		foreach( $post_types as $post_type )
			add_meta_box(
				'plainview_lane_booking',
				// Meta box title
				__( 'Plainview Lane Booking', 'pvlb' ),
				[ $this, 'show_meta_box' ],
				$post_type,
				'normal',
				'default'
			);
	}

	/**
		@brief		get_booking_editor
		@since		2019-03-31 20:59:25
	**/
	public function get_booking_editor( $state )
	{
		// No participants = don't display anything.
		if ( $state->participants()->all()->count() < 1 )
			return;

		$r = '<div id="plainview_lane_booking_editor">';
		$form = $this->form();
		$form->fieldset( 'fs_register' )
			->label( __( 'Schedule', 'pvlb' ) );
		$r .= $form->display_form_table();
		//$r .= '<h2>' . __( 'Schedule', 'pvlb' ) . '</h2>';

		// Add all participants.
		$r .= '<div class="participants column">';
		$r .= '<div class="column_heading">' . __( 'All participants', 'pvlb' ) . ' </div>';
		$r .= '<div class="content">';
		foreach( $state->participants()->all() as $participant_id => $participant )
			$r .= $participant->to_container_html();
		$r .= '</div>'; // content
		$r .= '</div>'; // column

		foreach( $state->groups as $group_id => $group )
		{
			$r .= sprintf( '<div class="group column group_%s" data-group_id="%s">', $group_id, $group_id );
			$r .= '<div class="column_heading">' . sprintf( __( 'Group %d', 'pvlb' ), $group_id ) . ' </div>';
			$r .= '<div class="content">';

			$r .= '<div class="active">';
			$r .= '<div class="column_subheading">' . sprintf( __( 'Shooters', 'pvlb' ), $group_id ) . ' </div>';
			foreach( $state->settings()->get( 'lane_numbers' ) as $lane_id )
			{
				$lane_identifier = new \plainview\pvlb_sdk\html\div();
				$lane_identifier->css_class( 'lane_identifier' );
				$lane_identifier->content( $lane_id );

				$lane = new \plainview\pvlb_sdk\html\div();
				$lane->css_class( 'empty' );
				$lane->css_class( 'lane' );
				$lane->css_class( 'selectable' );
				$lane->css_class( 'container' );
				$lane->css_class( 'active' );
				$lane->css_class( 'lane_' . $lane_id );
				$lane->set_attribute( 'data-group_id', $group_id );
				$lane->set_attribute( 'data-lane_id', $lane_id );
				$lane->content( $lane_identifier->toString() );

				$r .= $lane->toString();
			}
			$r .= '</div>'; // active

			$r .= '<div class="passive section">';
			$r .= '<div class="column_subheading">' . sprintf( __( 'Officials', 'pvlb' ), $group_id ) . ' </div>';
			$lane_id = 1;
			$lane_identifier = new \plainview\pvlb_sdk\html\div();
			$lane_identifier->css_class( 'lane_identifier' );
			$lane_identifier->content( $lane_id );

			$lane = new \plainview\pvlb_sdk\html\div();
			$lane->css_class( 'lane' );
			$lane->css_class( 'selectable' );
			$lane->css_class( 'container' );
			$lane->css_class( 'passive' );
			$lane->set_attribute( 'data-group_id', $group_id );
			$lane->set_attribute( 'data-lane_id', $lane_id );
			$lane->content( $lane_identifier->toString() );
			$r .= $lane->toString();
			$r .= '</div>'; // passive

			$r .= '</div>'; // content
			$r .= '</div>'; // column
		}

		$r .= '<ul class="tools">';
			// Add all shooters that need to loan guns to the editor
			$r .= '<li class="add_loaners"><span>' . __( 'Add loaners', 'pvlb' ) . '</span></li>';
			// Add all shooters to the editor
			$r .= '<li class="add_shooters"><span>' . __( 'Add shooters', 'pvlb' ) . '</span></li>';
			// Add all officials to the editor
			$r .= '<li class="add_officials"><span>' . __( 'Add officials', 'pvlb' ) . '</span></li>';
			// Remove all participants from the editor
			$r .= '<li class="remove_everybody"><span>' . __( 'Remove everybody', 'pvlb' ) . '</span></li>';
		$r .= '</ul>';

		$r .= '<script type="text/javascript">plainview_lane_booking_editor_state=' . $state->json_everything() . ';</script>';


		$r .= '</div>'; // plainview_lane_booking_editor

		$form = $this->form();
		$form->prefix( 'pvlb' );

		$form->select( 'schedule_action' )
			// Input description for the schedule
			->description( __( 'Send the e-mail schedule.', 'pvlb' ) )
			// Input label for the schedule
			->label( __( 'E-mail action', 'pvlb' ) )
			// Option for the schedule
			->opt( '', __( 'Do not send it', 'pvlb' ) )
			// Option for the schedule
			->opt( 'send_to_me', __( 'Send it to me', 'pvlb' ) )
			// Option for the schedule
			->opt( 'send_to_participants', __( 'Send it to the participants', 'pvlb' ) );

		$hidden_input = $form->hidden_input( 'plainview_lane_booking_editor_json' )
			->id( 'plainview_lane_booking_editor_json' );

		$r .= $form->display_form_table();

		return $r;
	}

	/**
		@brief		Add the settings to the form displayed to the event admin.
		@since		2019-03-31 20:35:06
	**/
	public function prepare_post_editor_form( $post_id, $state )
	{
		$form = $this->form();
		$form->prefix( 'pvlb' );

		if ( $this->debugging() )
		{
			$form->checkbox( 'delete_state' )
				->label( 'Delete the state' );
			$form->checkbox( 'add_10' )
				->label( 'Add 10 participants' );
		}

		$form->checkbox( 'enabled' )
			->checked( $state->settings()->is_enabled() )
			->description( __( 'Enable lane booking for this event.', 'pvlb' ) )
			->label( __( 'Enabled', 'pvlb' ) );

		$values = [];
		foreach( static::get_event_settings_keys() as $key )
			$values[ $key ] = $state->settings()->get( $key );

		$this->add_form_event_settings( $form, $values );

		$form->schedule_ready = $form->checkbox( 'schedule_ready' )
			->checked( $state->settings()->get_schedule_ready() )
			->description( __( "Display the schedule table on the event's page.", 'pvlb' ) )
			->label( __( 'Schedule ready', 'pvlb' ) );

		$form->fieldset( 'fs_register' )
			->label( __( 'Register a participant', 'pvlb' ) );
		$user_options = [
			'' => '',
		];
		foreach( get_users() as $user )
			$user_options[ $user->data->ID ] = $user->data->display_name;
		$form->select( 'user_to_register' )
			->label( __( 'Participant', 'pvlb' ) )
			->opts( $user_options );
		$options = $state->settings()->get( 'gun_classes' );
		asort( $options );
		$options = array_combine( $options, $options );
		$options = array_merge( [
			'passive' => __( 'Participate as an official', 'pvlb' ),
		], $options );
		$register_as = $form->select( 'register_as' )
			->label( __( 'Participate as', 'pvlb' ) )
			->opts( $options );

		if ( $state->participants()->all()->count() > 0 )
		{
			$form->fieldset( 'fs_unregister' )
				->label( __( 'Unregister a participant', 'pvlb' ) );
			$user_options = [
				'' => '',
			];
			foreach( $state->participants()->all() as $participant )
				$user_options[ $participant->get( 'id' ) ] = $participant->get( 'name' );
			$form->select( 'user_to_unregister' )
				->label( __( 'Participant', 'pvlb' ) )
				->opts( $user_options );
		}

		$action = $this->new_action( 'prepare_post_editor_form' );
		$action->form = $form;
		$action->post_id = $post_id;
		$action->state = $state;
		$action->execute();

		return $form;
	}

	/**
		@brief		Show the meta box.
		@since		2019-03-24 20:44:14
	**/
	public function show_meta_box()
	{
		global $post;
		$post_id = $post->ID;

		// Only save the data if the event has not passed.
		if ( State::too_old( $post_id ) )
		{
			echo __( 'This event has passed.', 'pvlb' );
			State::delete( $post_id );
			return false;
		}

		$state = State::load( $post_id );
		$form = $this->prepare_post_editor_form( $post_id, $state );

		echo $form->display_form_table();

		echo $this->get_booking_editor( $state );

		$this->wp_enqueue_scripts();
	}

	/**
		@brief		Maybe save the new settings.
		@since		2019-03-31 11:29:41
	**/
	public function save_post( $post_id )
	{
		if ( ! isset( $_POST[ 'pvlb' ] ) )
			return;

		$post = get_post( $post_id );
		if ( wp_is_post_revision( $post ) )
			return;

		$state = State::load( $post_id );

		$form = $this->prepare_post_editor_form( $post_id, $state );

		$hidden_input = $form->hidden_input( 'plainview_lane_booking_editor' );
		$schedule_action = $form->select( 'schedule_action' );

		$form->post();
		$form->use_post_values();

		$post_data = $_POST[ 'pvlb' ];
		unset( $_POST[ 'pvlb' ] );		// To prevent looping.

		if ( $this->debugging() )
		{
			if ( $form->checkbox( 'add_10' )->is_checked() )
			{
				// Find 10 users and add them as participants.
				for( $counter = 0; $counter < 10 ; $counter++ )
				{
					$users = get_users();
					$user = array_rand( $users );
					$user = $users[ $user ];

					$participant = $state->participants()->participant( $user->ID );

					$active = rand( 1, 20 ) > 5;
					if ( $active )
					{
						$state->participants()->add_active( $participant );

						// Find a gun class and assign it.
						$gun_classes = $state->settings()->get( 'gun_classes' );
						$gun_class = array_rand( $gun_classes );
						$gun_class = $gun_classes[ $gun_class ];
						$participant->set_gun_class( $gun_class );

						// Randomly decide whether the user wants to loan.
						$loan = rand( 1, 10 ) > 6;
						if ( $loan )
							$participant->set_gun_loan( $loan );
					}
					else
						$state->participants()->add_passive( $participant );
				}
				$state->participants()->sort_all();
			}
			if ( $form->input( 'delete_state' )->is_checked() )
			{
				State::delete( $post_id );
				return;
			}
		}

		if ( isset( $post_data[ 'plainview_lane_booking_editor_json' ] ) )
		{
			$lanes_json = $post_data[ 'plainview_lane_booking_editor_json' ];
			$lanes_json = stripslashes( $lanes_json );
			$lanes_json = json_decode( $lanes_json );

			if ( is_object( $lanes_json ) )
			{
				// Kill the groups contents.
				$state->groups()->init();
				foreach( $state->groups() as $group )
					$group->flush()->init();

				// Assign the participants.
				foreach( $lanes_json as $group_id => $types )
					foreach( $types as $type => $lanes )
						foreach( $lanes as $lane_id => $participant_id )
							$state->groups()->group( $group_id )->$type()->set( $lane_id, $participant_id );
			}
		}

		$values = $this->get_form_event_settings( $form );
		$values[ 'allow_gun_loan' ] = $form->checkbox( 'allow_gun_loan' )->is_checked();
		$values[ 'enabled' ] = $form->checkbox( 'enabled' )->is_checked();

		// We have to do some parsing to convert the user input into data we store.
		$values[ 'gun_classes' ] = $this->textarea_to_array( $values[ 'gun_classes' ] );
		$values[ 'lane_numbers' ] = explode( ' ', $values[ 'lane_numbers' ] );

		foreach( $values as $key => $value )
			$state->settings()->set( $key, $value );

		$value = $form->schedule_ready->get_filtered_post_value();
		$state->settings()->set_schedule_ready( $value );

		$user_to_register = $form->select( 'user_to_register' )->get_filtered_post_value();
		if ( $user_to_register > 0 )
		{
			$participant = $state->participants()->participant( $user_to_register );
			$register_as = $form->select( 'register_as' )->get_post_value();
			if ( $register_as == 'passive' )
				$state->participants()->add_passive( $participant );
			else
			{
				$participant = $state->participants()->add_active( $participant );
				$participant->set_gun_class( $register_as );
			}
			$state->participants()->sort_all();
		}

		$user_to_unregister = $form->select( 'user_to_unregister' )->get_filtered_post_value();
		if ( $user_to_unregister > 0 )
		{
			$state->participants()->remove_participant( $user_to_unregister );
		}

		$state->save();

		$to = [];
		switch( $schedule_action->get_post_value() )
		{
			case 'send_to_me':
				$user = wp_get_current_user();
				$to[ $user->data->user_email ] = $user->data->display_name;
			break;
			case 'send_to_participants':
				foreach( $state->participants()->all() as $participant )
				{
					$user_id = $participant->get( 'id' );
					$user = get_user_by( 'id', $user_id );
					$to[ $user->data->user_email ] = $user->data->display_name;
				}
			break;
		}
		if ( count( $to ) > 0 )
			$this->send_schedule_email( $state, $to );

		$action = $this->new_action( 'process_post_editor_form' );
		$action->form = $form;
		$action->post_id = $post_id;
		$action->state = $state;
		$action->execute();
	}
}
