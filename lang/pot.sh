find ../src -name *php > files.list
xgettext --omit-header -c --keyword=__ --language=PHP --from-code=UTF-8 -f files.list
rm files.list
cp messages.po messages.pot
