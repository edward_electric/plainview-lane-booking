(function($) {
  $.fn.shuffle = function() {
	// credits: http://bost.ocks.org/mike/shuffle/
	var m = this.length, t, i;

	while (m) {
	  i = Math.floor(Math.random() * m--);

	  t = this[m];
	  this[m] = this[i];
	  this[i] = t;
	}

	return this;
  };
}(jQuery));

/**
	@brief		The UI to create the schedule in the post editor.
	@since		2019-03-31 20:48:26
**/
;(function( $ )
{
	$.fn.extend(
	{
		plainview_lane_booking_editor : function()
		{
			return this.each( function()
			{
				var $this = $(this);
				$this.state = plainview_lane_booking_editor_state;		// Conv.
				$this.json_builder_timeout = false;
				$this.$group_lanes = false;
				$this.$participants = false;

				/**
					@brief		The selected lane;
					@since		2019-04-03 13:50:11
				**/
				$this.$selected_lane = false;

				/**
					@brief		Assign a participant to the selected lane.
					@since		2019-04-03 17:24:48
				**/
				$this.assign_participant = function( $participant, $lane )
				{
					var active = $lane.hasClass( 'active' );
					var group_id = $lane.data( 'group_id' );
					var group_id_key = 'group_' + group_id;

					// Is the participant assigned elsewhere in this group?
					if ( $participant.data( group_id_key ) !== false )
					{
						console.log( 'already assigned!' );
					}

					// Is the lane taken?
					var $lane_participant = $lane.data( 'participant' );
					if ( $lane_participant !== undefined )
						// The user cannot be in two places at once in a group.
						if ( $lane_participant.data( group_id_key ) !== false )
							$this.unassign_participant( $lane_participant, $lane );

					$participant.removeClass( 'unassigned' );

					var assignment_key = ( active ? 'assigned_active' : 'assigned_passive' );
					$participant.addClass( assignment_key );
					if ( active )
						$participant.data( assignment_key, $lane );
					else
					{
						// Passive assignments can be for several groups.
						var groups = $participant.data( assignment_key );
						if ( ! Array.isArray( groups ) )
							groups = [];
						groups[ group_id ] = $lane;
						$participant.data( assignment_key, groups );
					}

					// Add the participant to the lane.
					$lane.data( 'participant', $participant );
					$lane.removeClass( 'empty' );
					$lane.addClass( 'has_participant' );

					if ( ! active )
					{
						// Create a new, empty passive lane.
						var $clone = $lane.clone();
						var id = $clone.data( 'lane_id' );
						id = $( '.' + group_id_key + ' .passive.lane' ).length + 1;
						$clone.data( 'lane_id', id );
						$clone.removeClass( 'clickable' );		// Else it won't init.
						$clone.removeClass( 'selected' );
						$clone.appendTo( $lane.closest( '.passive.section' ) );
						$this.init_lanes();
					}

					var $p_clone = $participant.clone();
					$p_clone.data( 'participant',$participant );	// Save reference to original part.
					$p_clone.appendTo( $this.$selected_lane );
					$p_clone.data( 'lane', $this.$selected_lane );

					$p_clone.dblclick( function()
					{
						var $participant = $( this );
						$participant.data( 'lane' ).click();
						$participant.data( 'participant' ).click();
					} );
				}

				/**
					@brief		Build the json data that is sent to the server upon update.
					@since		2019-04-04 13:39:08
				**/
				$this.build_json = function()
				{
					var json = {};
					var $lanes = $( '.group .lane.container', $this );
					$.each( $lanes, function( index, item )
					{
						var $lane = $( item );

						// Is there a participant here?
						if ( $lane.data( 'participant' ) === undefined )
							return;

						var active = $lane.hasClass( 'active' );
						var active_passive = ( active ? 'active' : 'passive' );
						var group_id = $lane.data( 'group_id' );
						var lane_id = $lane.data( 'lane_id' );
						var participant_id = $lane.data( 'participant' ).data( 'id' );

						if ( json[ group_id ] === undefined )
							json[ group_id ] = {};
						if ( json[ group_id ][ active_passive ] === undefined )
							json[ group_id ][ active_passive ] = {};
						json[ group_id ][ active_passive ][ lane_id ] = participant_id;
					} );

					// And now save the json in the hidden input.
					json = JSON.stringify( json );
					$( '#plainview_lane_booking_editor_json' ).val( json );
				}

				/**
					@brief		Queue a build of the json.
					@since		2019-04-04 13:38:56
				**/
				$this.build_json_later = function()
				{
					clearTimeout( $this.json_builder_timeout );
					$this.json_builder_timeout = setTimeout( function()
					{
						$this.build_json();
					}, 100 );
				}

				/**
					@brief		Handles clicks to this lane.
					@since		2019-04-03 14:57:51
				**/
				$this.click_on_lane = function( $lane )
				{
					// Allow for unselecting.
					if ( $lane.hasClass( 'selected' ) )
					{
						$this.unselect_lane();
						return;
					}

					$this.$group_lanes.removeClass( 'selected' );
					$lane.addClass( 'selected' );

					$this.$selected_lane = $lane;
					$this.update_clickable_participants();
				}

				/**
					@brief		The user has clicked on a participant.
					@since		2019-04-03 15:33:13
				**/
				$this.click_on_participant = function( $participant )
				{
					if ( ! $participant.hasClass( 'clickable' ) )
						return;

					$this.build_json_later();

					var $lane_participant = $this.$selected_lane.data( 'participant' );
					if ( $lane_participant !== undefined )
					{
						// Unassign the participant from wherever he was.
						var same = $lane_participant.data( 'id' ) == $participant.data( 'id' );
			   			$this.unassign_participant( $participant, $this.$selected_lane );
						// Are we clicking on the part that was assigned to this lane?
						if ( same )
							return;
					}

					var group_id = $this.$selected_lane.data( 'group_id' );
					$this.remove_participant_from_group( $participant, group_id );

					$this.assign_participant( $participant, $this.$selected_lane );
					$this.select_next_lane();

				}

				/**
					@brief		Initialize the editor.
					@since		2019-04-03 13:01:07
				**/
				$this.init = function()
				{
					$this.init_lanes();
					$this.init_participants();
					$this.init_tools();
				}

				/**
					@brief		Go through the state and assign people to their lanes.
					@details	Also make lanes clickable.
					@since		2019-04-03 12:59:34
				**/
				$this.init_lanes = function()
				{
					// Find all html lanes
					$this.$group_lanes = $( '.group .lane.container', $this );
					$.each( $this.$group_lanes, function( index, lane )
					{
						var $lane = $( lane );
						if ( $lane.hasClass( 'clickable' ) )
							return;
						// Make it clickable.
						$lane.addClass( 'clickable' )
						.click( function()
						{
							var $lane = $( this );
							$this.click_on_lane( $lane );
						} );
					} );
				}

				/**
					@brief		Init the participants with clicking and stuff.
					@since		2019-04-03 15:29:00
				**/
				$this.init_participants = function()
				{
					$this.$participants = $( '.participants.column .participant' );
					$this.$participants.click( function()
					{
						var $participant = $( this );
						$this.click_on_participant( $participant );

					} );
					$this.update_clickable_participants();

					// Now load the state.
					$.each( $this.state.groups, function ( group_id, group )
					{
						$.each( group, function( type, lanes )
						{
							$.each( lanes, function( lane_id, participant_id )
							{
								if ( type == 'active' )
								{
									var $lane = $( '.group_' + group_id + ' .lane.lane_' + lane_id, $this );
								}
								else
								{
									// Select the first passive in the group and click that.
									var $lane = $( '.group_' + group_id + ' .lane.passive', $this ).last();
								}

								var $participant = $( '.participant_' + participant_id );
								$lane.click();
								$participant.click();
								$lane.click();		// Reselect the lane to counteract the next lane selection thing.
							} );
						} );
					} );
					$this.unselect_lane();
				}

				$this.init_tools = function()
				{
					$( '.tools .add_loaners span' ).click( function()
					{
						$this.maybe_select_first_empty_lane();
						$( '.participants .active.gun_loan.participant' ).shuffle().each( function( index, participant )
						{
							if ( ! $this.is_lane_selected() )
								return;
							var $participant = $( participant );
							// Don't assign assigned loaners.
							if ( $participant.hasClass( 'assigned_active' ) )
								return;
							$participant.click();
						} );
					} );

					$( '.tools .add_shooters span' ).click( function()
					{
						$this.maybe_select_first_empty_lane();
						$( '.participants .active.participant' ).shuffle().each( function( index, participant )
						{
							if ( ! $this.is_lane_selected() )
								return;
							var $participant = $( participant );
							// Don't assign loaners.
							if ( $participant.hasClass( 'gun_loan' ) )
								return;
							// Don't assign people who already have lanes.
							if ( $participant.hasClass( 'assigned_active' ) )
								return;
							$participant.click();
						} );
					} );

					$( '.tools .add_officials span' ).click( function()
					{
						$( '.group' ).each( function( index, group )
						{
							$( '.participants .passive.participant' ).each( function( index, participant )
							{
								var $participant = $( participant );
								var $lane = $( group ).find( '.lane.passive' ).last();
								$this.click_on_lane( $lane );
								$this.click_on_participant( $participant );
							} );
						} );
					} );

					$( '.tools .remove_everybody span' ).click( function()
					{
						$.each( $this.$participants, function( index, participant )
						{
							var $participant = $( participant );
							$this.remove_participant_from_all_groups( $participant );
						} );
						$this.build_json();
					} );
				}

				/**
					@brief		Is a lane selected?
					@since		2019-04-04 21:29:15
				**/
				$this.is_lane_selected = function()
				{
					return $this.$selected_lane !== false;
				}

				/**
					@brief		If no lane is selected, select the first empty lane.
					@since		2019-04-04 20:51:17
				**/
				$this.maybe_select_first_empty_lane = function()
				{
					if ( $this.is_lane_selected() )
						return;
					$this.select_first_empty_lane();
				}

				/**
					@brief		Remove this participant from all groups.
					@since		2019-04-04 13:25:31
				**/
				$this.remove_participant_from_all_groups = function( $participant )
				{
					for ( group_id in $this.state.groups )
						$this.remove_participant_from_group( $participant, group_id );
				}

				/**
					@brief		Remove the user from this group, if applicable.
					@since		2019-04-04 12:57:58
				**/
				$this.remove_participant_from_group = function( $participant, group_id )
				{
					var $active_lane = $participant.data( 'assigned_active' );
					if ( $active_lane !== undefined )
						if ( $active_lane.data( 'group_id' ) == group_id )
						{
							$this.unassign_participant( $participant, $active_lane );
						}

					var groups = $participant.data( 'assigned_passive' );
					if ( Array.isArray( groups ) )
						if ( groups[ group_id ] !== undefined )
							$this.unassign_participant( $participant, groups[ group_id ] );
				}

				/**
					@brief		Select the first available lane.
					@since		2019-04-04 20:12:33
				**/
				$this.select_first_empty_lane = function()
				{
					$this.unselect_lane();
					$( '.lane.container.empty' ).first().click();
				}

				/**
					@brief		Select the next free lane.
					@since		2019-04-03 15:40:03
				**/
				$this.select_next_lane = function()
				{
					var $next = $this.$selected_lane.nextAll( '.empty' )
						.first();
					if ( $next.length > 0 )
						$next.click();
					else
						$this.unselect_lane();
				}

				/**
					@brief		Remove a participant from an assigned lane.
					@since		2019-04-03 17:27:12
				**/
				$this.unassign_participant = function( $participant, $lane )
				{
					var active = $lane.hasClass( 'active' );
					var assignment_key = ( active ? 'assigned_active' : 'assigned_passive' );
					var group_id = $lane.data( 'group_id' );
					var remove_from_lane = true;
					if ( active )
					{
						$participant.addClass( 'unassigned' );
						// Since the part can only be active in one place...
						$participant.removeClass( assignment_key );
						$participant.removeData( assignment_key );
					}
					else
					{
						// Passive assignments can be for several groups.
						var groups = $participant.data( assignment_key );
						if ( groups !== undefined )
						{
							groups.splice( group_id, 1 );
							groups = groups.filter(function () { return true });
							if ( groups.length < 1 )
							{
								$participant.removeClass( assignment_key );
								$participant.removeData( assignment_key );
								$participant.addClass( 'unassigned' );
							}
							else
								$participant.data( assignment_key, groups );
						}

						// Only remove the passive part
						var $passive_section = $lane.closest( '.passive.section' );
						if ( $( '.lane.container', $passive_section ).length > 1 )
						{
							remove_from_lane = false;
							$lane.remove();
						}
					}

					if ( remove_from_lane )
					{
						// And now remove the participant from the lane.
						$( '.participant.container', $lane ).remove();
						$lane.removeData( 'participant' );
						$lane.addClass( 'empty' );
						$lane.removeClass( 'has_participant' );
					}
				}

				/**
					@brief		Unselect the lane.
					@since		2019-04-04 20:18:46
				**/
				$this.unselect_lane = function()
				{
					if ( ! $this.is_lane_selected() )
						return;
					$this.$selected_lane.removeClass( 'selected' );
					$this.$selected_lane = false;
					$this.update_clickable_participants();
				}

				/**
					@brief		Update which participants can be clicked.
					@since		2019-04-03 15:14:48
				**/
				$this.update_clickable_participants = function()
				{
			   		$this.$participants.removeClass( 'clickable' );

					if ( $this.$selected_lane == false )
						return;

			   		var active = $this.$selected_lane.hasClass( 'active' );
			   		if ( active )
			   			// If an active lane, only active parts may be added.
			   			$( '.participants.column .participant.active' ).addClass( 'clickable' );
			   		else
			   			$( '.participants.column .participant' ).addClass( 'clickable' );
				}

				$this.init();

			} ); // return this.each( function()
		} // plugin: function()
	} ); // $.fn.extend({
} )( jQuery );
;
jQuery( document ).ready( function( $ )
{
	$( '#plainview_lane_booking_editor' ).plainview_lane_booking_editor();
} );
;
